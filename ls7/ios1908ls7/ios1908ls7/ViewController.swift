//
//  ViewController.swift
//  ios1908ls7
//
//  Created by Wa on 23.09.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var barButtonItem: UIBarButtonItem!
    @IBOutlet var menuButton: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ViewDidLoad")
        barButtonItem.menu = UIMenu(title: "TEst", image: nil, identifier: nil, options: .destructive, children: [
            UIAction(title: "TEst 1", image: nil, identifier: .none, discoverabilityTitle: nil, attributes: [], state: .on, handler: { _ in
                print("BUTTON PRESSED")
            }),
            UIAction(title: "TEst 2", image: nil, identifier: .none, discoverabilityTitle: nil, attributes: [], state: .off, handler: { _ in
                print("BUTTON 2 PRESSED")
            })
        ])
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("ViewWillAppear")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("VIewDIDAppear")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("ViewWillDisappear")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("ViewDidDisappear")
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        super.shouldPerformSegue(withIdentifier: identifier, sender: sender)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        // CHECK THAT SEGUE HAS IDENTIFIER THAT WE EXPECT
        guard segue.identifier == "goToBlueVC" else { return }
        // CAST DESTINATION AS CHILD CLASS
        guard let destinationVC = segue.destination as? BluewViewController else { return }
        destinationVC.stepperValue = stepper.value
//        destinationVC.delegate = self
        destinationVC.didEndEditing = { [weak self] text in
            self?.mainLabel.text = text
        }
//        destinationVC.stepperValueLabel.text = "STEPPER VALUE: \(stepper.value)"
    }

    @IBAction func barButtonAction(_ sender: UIBarButtonItem) {
        let viewController = UIViewController()
        viewController.view.backgroundColor = .red
        present(viewController, animated: true, completion: nil)
    }

    @IBAction func stepperAction(_ sender: UIStepper) {
        if sender.value == 4.0 {
            print("GO TO NEXT PAGE")
            // IF SEGUE EXIST
            performSegue(withIdentifier: "goToBlueVC", sender: nil)
            // NO SEGUE
//            let viewController = UIViewController()
//            viewController.view.backgroundColor = .red
//            present(viewController, animated: true, completion: nil)
        }
    }
    
    deinit {
        print("ROOT VIEW CONTROLLER DEINITED")
    }
}

extension ViewController: BluewViewControllerDelegate {
    func userDidFinishTyping(text: String) {
        print("RECEIVED", text)
    }
}
