//
//  Human.swift
//  ios1908ls7
//
//  Created by Wa on 23.09.2021.
//

import Foundation

enum Spell {
    case fireBall
}

class Human {
    
}

class Orc {
    
}

class Dwarf: Wizzard {
    let height: Double = 50.0

    func castSpell(spell: Spell) {
        
    }
}

protocol Wizzard {
    func castSpell(spell: Spell)
}

protocol Warrior {
    func attack()
}
