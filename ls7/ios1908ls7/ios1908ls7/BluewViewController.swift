//
//  BluewViewController.swift
//  ios1908ls7
//
//  Created by Wa on 23.09.2021.
//

import UIKit

protocol BluewViewControllerDelegate: AnyObject {
    func userDidFinishTyping(text: String)
}

class BluewViewController: UIViewController {
    
    var stepperValue: Double = 0
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var stepperValueLabel: UILabel!
    
    weak var delegate: BluewViewControllerDelegate?
    var didEndEditing: ((String) -> Void)?
    
    override func loadView() {
        super.loadView()
        print("BLUE LOAD VIEW")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("BLUE VIEW DID LOAD")
        stepperValueLabel.text = "STEPPER VALUE: \(stepperValue)"
        textField.delegate = self
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("BLUE ViewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("BLUE ViewDidAppear")
        print("CURRENT STEPER VAL", stepperValue)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("BLUE ViewWillDisAppear")
    }

    @IBAction func textfFieldEditingDidEndAction(_ sender: UITextField) {
        delegate?.userDidFinishTyping(text: sender.text ?? "")
    }
    
    @IBAction func textFieldEditingChangedAction(_ sender: UITextField) {
        if let text = sender.text, text.contains("close") {
            guard navigationController != nil else {
                dismiss(animated: true, completion: nil)
                return
            }
            // REMOVE CURRENT CONTROLLER
            navigationController?.popViewController(animated: true)
        }
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        navigationController?.setViewControllers([self], animated: true)
//        delegate?.userDidFinishTyping(text: textField.text ?? "")
//        view.endEditing(true)
//    }

    deinit {
        print("BLUE DEINIT")
    }
}

extension BluewViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(textField.text, string)
        guard let text = textField.text else { return true }
        return text.count <= 8
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        navigationController?.setViewControllers([self], animated: true)
        delegate?.userDidFinishTyping(text: textField.text ?? "")
        
        didEndEditing?(textField.text ?? "")
        textField.endEditing(true)
        return true
    }
}
