//
//  RandomViewController.swift
//  ios1908ls7
//
//  Created by Wa on 23.09.2021.
//

import UIKit

class RandomViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .random()
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        // MANUAL VIEW CONTROLLER CREATION ----- WITHOUT UI FROM STORYBOARD
//        let viewController = RandomViewController()
//        present(viewController, animated: true, completion: nil)
        
        
        // MANUAL VIEW CONTROLLER CREATION ----- WITH UI FROM STORYBOARD
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "RandomViewController") else { return }
        // MODAL
//        present(controller, animated: true, completion: nil)
        // ADD TO NAVIGATION STACK
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
           alpha: 1.0
        )
    }
}
