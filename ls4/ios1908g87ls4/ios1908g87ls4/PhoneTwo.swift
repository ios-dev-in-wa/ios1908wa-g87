import UIKit

enum PhoneModel {
    case eleven, elevenPro, elevenProMax
}

class Phone {
    let brand: String
    let model: PhoneModel
    let color: UIColor
    var phoneNumbers: [String: Int]
    
    init(brand: String, model: PhoneModel, color: UIColor, phoneNumbers: [String: Int]) {
        self.brand = brand
        self.model = model
        self.color = color
        self.phoneNumbers = phoneNumbers
    }
    
    func clock() {
        print("tututututututu")
    }
    func message() {
        print("Hi sweetie")
    }
    func reminder() {
        print("Time to go")
    }
    
    func addContact(name: String, number: Int) {
        phoneNumbers[name] = number
    }
    
    func deleteContact(name: String) {
        phoneNumbers[name] = nil
    }
}
