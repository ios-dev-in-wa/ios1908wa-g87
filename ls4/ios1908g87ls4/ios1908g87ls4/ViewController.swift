//
//  ViewController.swift
//  ios1908g87ls4
//
//  Created by Wa on 02.09.2021.
//

import UIKit

class ViewController: UIViewController {
    
    var carOne: Car? = Car(brandName: "Mazda", modelName: "3", color: .blue, bodyType: .coope)
    let battleCarOne = BattleCar(ammoCount: 200)
    var currentHuman: Human?

    override func viewDidLoad() {
        super.viewDidLoad()
//        let phone: Phone = Phone(brand: <#T##String#>, model: <#T##PhoneModel#>, color: <#T##UIColor#>, phoneNumbers: <#T##[String : Int]#>)
        
        let carTwo = Car(brandName: "Lada", modelName: "9", color: .brown, bodyType: .sedan)

        carOne?.startEngine()
        carOne?.moveForward()

        battleCarOne.moveForward()
        
//        test - won't work

//        func moveee() {
//            carOne
//            let test = "132"
//        }
//        carOne = nil
//        carPark()

        if let car = carOne {
            let humanOne = Human(name: "Artem", surname: "Velykyy", car: battleCarOne)
            currentHuman = humanOne
        }
        currentHuman?.preprareForWork()

//        for i in "Artem" {
//            if i.
//        }
    }

    func carPark() {
        carOne?.parking()
    }

}

