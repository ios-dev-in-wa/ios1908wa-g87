//
//  Car.swift
//  ios1908g87ls4
//
//  Created by Wa on 02.09.2021.
//

import UIKit

enum BodyType {
    case sedan, coope, hatchback
}

class Car {
    let brandName: String
    let modelName: String
    let color: UIColor
    let bodyType: BodyType 
    
    init(brandName: String, modelName: String, color: UIColor, bodyType: BodyType) {
        self.brandName = brandName
        self.modelName = modelName
        self.color = color
        self.bodyType = bodyType
    }

    init(modelName: String, color: UIColor, bodyType: BodyType) {
        brandName = "BMW"
        self.modelName = modelName
        self.color = color
        self.bodyType = bodyType
    }

    func startEngine() {
        print("trrrrrtrtrrtrt")
    }

    func stopEgine() {
        print("Ptch...")
    }

    func moveForward() {
        print("\(brandName + modelName) moving forward")
    }

    func parking() {
        print("\(brandName + modelName) parked")
    }

    func doorsCount() -> Int {
        switch bodyType {
        case .coope: return 2
        case .sedan: return 4
        case .hatchback: return 5
        }
    }
}
