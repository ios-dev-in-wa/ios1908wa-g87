//
//  Human.swift
//  ios1908g87ls4
//
//  Created by Wa on 02.09.2021.
//

import Foundation

class Human {
    let name: String
    let surname: String
    let car: Car

    init(name: String, surname: String, car: Car) {
        self.name = name
        self.surname = surname
        self.car = car
    }

    func preprareForWork() {
        commitMessage(msg: "Wake up")
        commitMessage(msg: "brash teeth")
        commitMessage(msg: "make a lunch")
        commitMessage(msg: "go to parking")
        car.startEngine()
        car.moveForward()
        commitMessage(msg: "go to office")
    }

    func commitMessage(msg: String) {
        print("\(name) \(msg)")
    }
}
