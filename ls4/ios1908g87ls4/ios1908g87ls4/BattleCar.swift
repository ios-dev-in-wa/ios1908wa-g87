//
//  BattleCar.swift
//  ios1908g87ls4
//
//  Created by Wa on 02.09.2021.
//

import UIKit

enum GunType {
    case minigun, rocketLauncher
}

class BattleCar: Car {
    let gunType: GunType = .minigun
    let ammoCount: Int

    init(ammoCount: Int) {
        self.ammoCount = ammoCount
        super.init(brandName: "Hammer", modelName: "H1", color: .brown, bodyType: .hatchback)
    }

    override init(brandName: String, modelName: String, color: UIColor, bodyType: BodyType) {
        ammoCount = 100
        super.init(brandName: brandName, modelName: modelName, color: color, bodyType: bodyType)
    }

    override func moveForward() {
        super.moveForward()
        print("BattleCar \(brandName) is moving to location")
    }
}
