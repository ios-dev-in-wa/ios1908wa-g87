//
//  Phone.swift
//  ios1908g87ls4
//
//  Created by Wa on 02.09.2021.
//

import UIKit

enum PhoneBook {
    case name, surName, phone
}

class Phone {
    let model: String
    let hasScreen: Bool
    let sizeOfScreen: Int
    let color: UIColor
    var phoneBook: [String: String]

    // Computed property
    var hasPhoneBook: Bool {
        return !phoneBook.isEmpty
    }
    
    init(model: String, hasScreen: Bool, sizeOfScreen: Int, color: UIColor, phoneBook: [String: String]) {
        self.model = model
        self.hasScreen = hasScreen
        self.sizeOfScreen = sizeOfScreen
        self.color = color
        self.phoneBook = phoneBook
    }
    
    func ringinToLoud () {
        print("\(model) is louded too much")
    }

    func addContact(name: String, phone: String) {
        if let number = phoneBook[name], !number.isEmpty {
            return
        }
        
        phoneBook[name] = phone
    }

    func removeContact(name: String) {
        phoneBook.removeValue(forKey: name)
    }
}
