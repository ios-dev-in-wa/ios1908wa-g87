//
//  Sesanoble.swift
//  ios1908ls6
//
//  Created by Wa on 21.09.2021.
//

import UIKit

protocol Seasonable {
    func winterIsComing()
    func springIsComing()
    func summerIsComing()
    func autumnIsComing()
}

protocol Namable {
    var name: String { get }
}

class Bear: Seasonable, Namable {
    var name: String = "Gummy"
    var isInSpiachka: Bool = false

    func winterIsComing() {
        print("Bear \(name) going to Spiachka")
        isInSpiachka = true
    }
    
    func springIsComing() {
        isInSpiachka = false
    }
    
    func summerIsComing() {
        isInSpiachka = false
    }
    
    func autumnIsComing() {
        isInSpiachka = false
    }
}


class Rabbit: Namable, Seasonable {
    var name: String = "Bunny"
    var furColor: UIColor = .white
    
    func winterIsComing() {
        print("Rabbit \(name) changing fur to White")
        furColor = .white
    }
    
    func springIsComing() {
        furColor = .gray
    }
    
    func summerIsComing() {
        furColor = .gray
    }
    
    func autumnIsComing() {
        furColor = .gray
    }
    
    
}
