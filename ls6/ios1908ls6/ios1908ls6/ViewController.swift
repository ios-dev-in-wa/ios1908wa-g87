//
//  ViewController.swift
//  ios1908ls6
//
//  Created by Wa on 21.09.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var segmentControll: UISegmentedControl!
    @IBOutlet weak var segmentSwitch: UISwitch!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var progressView: UIProgressView!
    let names = ["Artem", "Ihor", "Alena", "Sergey"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let car = Car()
        textField.becomeFirstResponder()
        let bear = Bear()
        let rabbit = Rabbit()

        let seasonableAnimals: [Seasonable] = [bear, rabbit]

        for animal in seasonableAnimals {
            animal.winterIsComing()
        }
        
        
        let namable: [Namable] = [bear, rabbit]
        for animal in namable {
            print(animal.name)
        }
    }

    @IBAction func textFieldValueChangedAction(_ sender: UITextField) {
        print(sender.text)
    }
    
    @IBAction func textFieldEdittingBeginAction(_ sender: UITextField) {
        print("EDIT BEGIN, \(sender.text)")
    }
    
    @IBAction func textFieldEditingChangedAction(_ sender: UITextField) {
        print("Editing change, \(sender.text)")
        if let text = sender.text {
            if text.count <= 8 {
                progressView.progress = Float(text.count) / Float(8)
            }
            segmentSwitch.isEnabled = text.count >= 8
        }
    }
    
    @IBAction func textFieldEditingEndAction(_ sender: UITextField) {
        print("Editing end, \(sender.text)")
    }

    @IBAction func loginAction(_ sender: UIButton) {
//        if let text = textField.text, text.count >= 8 {
//            statusLabel.isHidden = true
//            print("Login with ", text)
//        } else {
//            statusLabel.isHidden = false
//            statusLabel.text = "Enter more than 8 symbols"
//        }
        //////
        guard let text = textField.text else {
            statusLabel.isHidden = false
            statusLabel.text = "Enter more than 8 symbols"
            activityIndicatorView.startAnimating()
            return
        }
        let stringRegex = "^(?=.\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$"
        
        do {
            let reg = try NSRegularExpression(pattern: stringRegex, options: [])
            let range = NSRange(location: 0, length: text.count)
            if let val = reg.firstMatch(in: text, options: [], range: range) {
                print(val)
                let start = val.range.location
                let finhis = val.range.location + val.range.length
                let range = Range(start...finhis - 1)
                print(text.substringRange(with: range))
            }
        } catch {
            print(error.localizedDescription)
        }
        activityIndicatorView.stopAnimating()
        statusLabel.isHidden = true
        print("Login with ", text)
    }

    @IBAction func stepperValueChangedAction(_ sender: UIStepper) {
        print(sender.value)
    }
    
    @IBAction func segmentValueChangedAction(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        textField.text = names[sender.selectedSegmentIndex]
    }
    
    @IBAction func switchValueChangedAction(_ sender: UISwitch) {
        segmentControll.isEnabled = sender.isOn
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substringRange(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}
