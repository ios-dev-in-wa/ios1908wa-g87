//
//  ViewController.swift
//  ios1908ls16
//
//  Created by Wa on 26.10.2021.
//

import UIKit

class ViewController: UIViewController {
    
    let textFieldView: HikingTextFieldView = .fromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.addSubview(textFieldView)
        textFieldView.frame = CGRect(x: 20, y: 50, width: 300, height: 75)
        textFieldView.setupWith(image: UIImage(systemName: "person")!, textFieldPlaceholder: "Login", isEditable: true, delegate: self)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension ViewController: HikingTextFieldViewDelegate {
    func textFieldDidEndEditing(text: String) {
        print("Received", text)
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
