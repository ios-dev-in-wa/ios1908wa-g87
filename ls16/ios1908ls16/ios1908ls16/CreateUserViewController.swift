//
//  CreateUserViewController.swift
//  ios1908ls16
//
//  Created by Wa on 26.10.2021.
//

import UIKit

class CreateUserViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    @IBOutlet weak var isActiveSwitch: UISwitch!

    private lazy var userService: UserService = {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        return UserService(context: appDelegate.container.viewContext)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        userService.readUsers { users in
            users.forEach { user in
                print(user.name)
            }
        }

        userService.getUserByName(name: "Artem") { users in
            users.forEach { user in
                print("Artems", user)
            }
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        userService.createUser(name: nameTextField.text!, surname: surnameTextField.text!, gender: genderSegmentControl.titleForSegment(at: genderSegmentControl.selectedSegmentIndex)!, isActive: isActiveSwitch.isOn)
    }
    
}
