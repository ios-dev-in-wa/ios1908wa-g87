//
//  UserService.swift
//  ios1908ls16
//
//  Created by Wa on 26.10.2021.
//

import CoreData

class UserService {
    private let context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }

    func readUsers(completion: (([User]) -> Void)) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")

        do {
            let result = try context.fetch(fetchRequest) as? [User]
            completion(result ?? [])
        } catch {
            print(error.localizedDescription)
        }
    }

    func createUser(name: String, surname: String, gender: String, isActive: Bool) {
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context)!
        guard let user = NSManagedObject(entity: entity, insertInto: context) as? User else { return }

        user.name = name
        user.surname = surname
        user.gender = gender
        user.isActive = isActive

        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }

    }

    func getUserByName(name: String, completion: (([User]) -> Void)) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)

        do {
            let result = try context.fetch(fetchRequest) as? [User]
            completion(result ?? [])
        } catch {
            print(error.localizedDescription)
        }
    }

    func updateUser(user: User) {
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }

    func deleteUser(user: User) {
        context.delete(user)
    }
}
