//
//  HikingTextFieldView.swift
//  ios1908ls16
//
//  Created by Wa on 26.10.2021.
//

import UIKit

protocol HikingTextFieldViewDelegate: AnyObject {
    func textFieldDidEndEditing(text: String)
}

class HikingTextFieldView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textFieldImageView: UIImageView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var editButton: UIButton!
    
    private var isInEdit = false {
        didSet {
            setEditStyle()
        }
    }
    private weak var delegate: HikingTextFieldViewDelegate?
    
    func setupWith(image: UIImage, textFieldPlaceholder: String, isEditable: Bool, delegate: HikingTextFieldViewDelegate) {
        self.delegate = delegate
        setupUI()
    }

    private func setupUI() {
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 27.5
        setEditStyle()

        textField.delegate = self
    }

    private func setEditStyle() {
        containerView.layer.borderColor = isInEdit ? UIColor.yellow.cgColor : UIColor.green.cgColor
        editButton.isHidden = isInEdit
        textField.isEnabled = isInEdit
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        isInEdit.toggle()
        if isInEdit {
            textField.becomeFirstResponder()
        }
    }
}

extension HikingTextFieldView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        isInEdit = false
        delegate?.textFieldDidEndEditing(text: textField.text ?? "")
    }
}
