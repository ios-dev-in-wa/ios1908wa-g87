//
//  User.swift
//  ios1908ls16
//
//  Created by Wa on 26.10.2021.
//

import Foundation

class UserT {
    var name: String
    var surname: String
    var gender: String
    var isActive: Bool

    init(name: String, surname: String, gender: String, isActive: Bool) {
        self.name = name
        self.surname = surname
        self.gender = gender
        self.isActive = isActive
    }
}
