//
//  ViewController.swift
//  ios1908ls5
//
//  Created by Wa on 07.09.2021.
//

import UIKit

class CountingModel {
    private var counter = 0

    func incerease() -> Int {
        counter += 1
        return counter
    }
}
// IPHONE 12 ONLY
class ViewController: UIViewController {

    @IBOutlet weak var loremLabel: UILabel!
    @IBOutlet weak var tapMeButton: UIButton!

    let model = CountingModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        stringSeparation(fullname: "IvanVasilievich")
//        view.backgroundColor = .red
        updateHelloLabel(count: 0)
        loremLabel.textAlignment = .center
    }

    func stringSeparation(fullname: String) {
        var bigLetter = ""
        for i in fullname.reversed() {
            if i.isUppercase {
                bigLetter = "\(i)"
                break
            }
        }
        let splitted = fullname.split(separator: Character(bigLetter))
        if let firstName = splitted.first,
           let surname = splitted.last {
            print("First name: \(firstName)")
            print("Last name: \(bigLetter + surname)")
            print("Full name: \(firstName) \(bigLetter + surname)")
            print("---------------")
            print("Last name", fullname.replacingOccurrences(of: firstName, with: ""))
        }
    }

    func updateHelloLabel(count: Int) {
        loremLabel.text = "Hello world \(count)"
    }
    
    @IBAction func tapMeAction(_ sender: UIButton) {
        let count = model.incerease()
        print(count)
        updateHelloLabel(count: count)
    }
}
 
