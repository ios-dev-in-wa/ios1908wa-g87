//
//  LoginViewController.swift
//  ios1908ls5
//
//  Created by Wa on 07.09.2021.
//

import UIKit

enum Drink {
    case americano, latte

    var coffeeNeeded: Double {
        switch self {
        case .americano:
            return 30.0
        case .latte:
            return 35.0
        }
    }

    var milkNeeded: Double {
        switch self {
        case .americano:
            return 0
        case .latte:
            return 50.0
        }
    }
}

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var statusLabel: UILabel!
    
    let model = LoginModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // IN ACTION
//        model.makeDrink(drink: .latte)
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        if let username = loginTextField.text,
           let password = passwordTextField.text,
           !username.isEmpty,
           !password.isEmpty {
            let status = model.authorize(username: username, password: password)
            statusLabel.textColor = status ? .green : .red
            statusLabel.text = status ? "User authorized" : "Username/password is wrong"
        } else {
            statusLabel.textColor = .red
            statusLabel.text = "Please enter username and password"
        }
        statusLabel.isHidden = false
    }
    
    // FUNC IN MODEL
    func makeDrink(drink: Drink) {
//        drink.coffeeNeeded
//        drink.milkNeeded
    }
    
}
