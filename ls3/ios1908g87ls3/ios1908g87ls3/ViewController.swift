//
//  ViewController.swift
//  ios1908g87ls3
//
//  Created by Wa on 31.08.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("hello")
//        bringBackExample()
//        test(a: 10)
//        baseTypes()
//        arrays()
        dictExample()
    }

    
    func baseTypes() {
        let int = 1
        let double = 2.0

        print(Double.pi)
    
        var name = "Artem 234"

        for i in name {
            print(i)
        }

        print(name.count)

        name.append(" mentor 😀")
        name += " and here we go"
        print(name)
//        "s"

        let newName = name.replacingOccurrences(of: "o", with: "O")
        print(newName)
    }

    func arrays() {
        var names: [String] = []
        names.append("Artem")
        names.append("Artem")
        names.append("Ihor")
        names.append("Anna")
        print(names)

//        print(names[2])

        // OPTIONAL BINDING
        if let lastElement = names.last {
            print(lastElement)
        }

        names.insert("Olena", at: 0)
        print(names)

//        names.remove(at: 1)
        print(names)

        for name in names {
            print("\nHello, \(name)")
        }

        if names.count > 5 {
            print(names[4])
            // last element (names.count - 1)
        }

//        if names.contains("Artem") == false {
        if !names.contains("Artem") {
            names.append("Artem")
        }

        print(names.randomElement())
        
        print(names.sorted(by: <))

        let sortedByCount = names.sorted { first, next in
            return first.count > next.count
        }

        names.sorted { first, next in
            return first.count < next.count
        }
        print(sortedByCount)

        let onlyANames = names.filter { element in
            return element.contains("A")
        }
        print(onlyANames)

        let numbers = [0, 20, 2, 5, 100, 35]

        print(numbers.sorted(by: <))
        let sorted = numbers.sorted { first, next in
            print(first, next)
            return first > next
        }
        print(sorted)

        print(names.firstIndex(of: "Artem"))

        let artems = names.filter { names in
            return names == "Artem"
        }
        print(artems)
    }

    func dictExample() {
        var dict = [
            "Police": 911,
            "Embulance": 98765
        ]

        dict["Firefigters"] = 989

        print(dict["Police"])
        print(dict)
        print(dict["home"])

        print(dict.keys)
        print(dict.values)

        let dictEx = [
            "1-A": ["Artem", "Olena", "Jora"],
            "2-B": ["Ilona", "Alla", "Sergey"]
        ]
        print(dictEx)

        if let firstStudent = dictEx["1-A"]?.first {
            print("Go to blackboard \(firstStudent)")
        }

        let dictNumber = [
            0: "Zero",
            1: "One",
            2: "Two"
        ]

        let intArray = [0, 1, 2, 0, 3, 1]

        for i in intArray {
            print(dictNumber[i])
        }
        let setInt = Set(intArray)
//        setInt
        
    }

    func test(a: Int) {
        for i in 0...a {
            print(i)
        }
    }
    
    func bringBackExample() {
        let box = UIView()
        box.backgroundColor = .red
        box.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        box.center = view.center
        view.addSubview(box)

        let boxTwo = UIView()
        boxTwo.backgroundColor = .brown
        boxTwo.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        boxTwo.center = view.center
        view.addSubview(boxTwo)

        view.bringSubviewToFront(box)
//        view.sendSubviewToBack(boxTwo)
    }


}

