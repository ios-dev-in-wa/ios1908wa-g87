//
//  GestureViewController.swift
//  ios1908ls8
//
//  Created by Wa on 28.09.2021.
//

import UIKit

class GestureViewController: UIViewController {

    @IBOutlet weak var blackView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        print("LOCATION", sender.location(in: view))
        logGesture(gesture: sender)
    }

    @IBAction func panGestureAction(_ sender: UIPanGestureRecognizer) {
        print("Vel", sender.velocity(in: view))
        print("Translation", sender.translation(in: view))
        print("LOCATION", sender.location(in: view))
        blackView.center = sender.location(in: view)
        logGesture(gesture: sender)
    }

    func logGesture(gesture: UIGestureRecognizer) {
        switch gesture.state {
        case .possible:
            print("\(String(describing: type(of: gesture))) Possible")
        case .began:
            print("\(String(describing: type(of: gesture))) Began")
        case .changed:
            print("\(String(describing: type(of: gesture))) Changed")
        case .ended:
            print("\(String(describing: type(of: gesture))) Ended")
        case .cancelled:
            print("\(String(describing: type(of: gesture))) Cancelled")
        case .failed:
            print("\(String(describing: type(of: gesture))) Failed")
        @unknown default:
            fatalError()
        }
    }
    
    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
        print("Scale", sender.scale)
        print("Vel Scale", sender.velocity)
        logGesture(gesture: sender)
    }

    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        print("Rotation", sender.rotation)
        print("Vel Rotation", sender.velocity)
        logGesture(gesture: sender)
    }
    
    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        logGesture(gesture: sender)
    }
    
    @IBAction func longAction(_ sender: UILongPressGestureRecognizer) {
        
        logGesture(gesture: sender)
    }
}
