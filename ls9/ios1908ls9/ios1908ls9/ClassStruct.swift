//
//  ClassStruct.swift
//  ios1908ls9
//
//  Created by Wa on 30.09.2021.
//

import Foundation

protocol Test {
    func helloPrint()
}

// VALUE TYPE
struct FruitStruct: Test {
    var name: String
    let expirationDate: Date
    let isAccessible: Bool

    func helloPrint() {
        
    }
}

// PROHIBITED
//struct NewFruitStruct: FruitStruct {
//
//}


// REFERENCE TYPE
class FruitClass: Test {
    var name: String
    let expirationDate: Date

    init(name: String, expirationDate: Date) {
        self.name = name
        self.expirationDate = expirationDate
    }
    
    func helloPrint() {
        
    }
}
// SUBCLASSING IS ONLY FOR CLASSES

class NewFruit: FruitClass {
    
}
