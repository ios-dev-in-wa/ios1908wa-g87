//
//  ViewController.swift
//  ios1908ls9
//
//  Created by Wa on 30.09.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
//    var fruits: [String] = [
//        "Orange",
//        "Apple",
//        "Melon",
//        "Strawberry"
//    ]
    
    var fruits: [FruitStruct] = [
        FruitStruct(name: "Melon", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Apple", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Chery", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Strawberry", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Banana", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Melon", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Apple", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Chery", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Strawberry", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Banana", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Melon", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Apple", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Chery", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Strawberry", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Banana", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Melon", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Apple", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Chery", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Strawberry", expirationDate: Date(), isAccessible: true),
        FruitStruct(name: "Banana", expirationDate: Date(), isAccessible: true)
    ]

    @IBOutlet weak var cellSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        
        let fruitStruct = FruitStruct(name: "Melon", expirationDate: Date(), isAccessible: true)
        let fruitClass = FruitClass(name: "Melon", expirationDate: Date())
        
        var copyStruct = fruitStruct
        copyStruct.name = "MELON2"
        
        
        let copyClass = fruitClass
        copyClass.name = "Melon2"
        
        
        print(fruitStruct, fruitClass.name)
        print("-------")
        print(copyStruct, copyClass.name)
    }


    @IBAction func cellSwitchValueChanged(_ sender: UISwitch) {
        
    }

    @IBAction func editButtonAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        sender.title = tableView.isEditing ? "Cancel" : "Edit"
    }
}

extension ViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return indexPath.row % 2 == 0 ? 100 : 50
//    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = .red
//        let label = UILabel()
//        label.text = "\(section) Section"
//        headerView.addSubview(label)
//        label.center = headerView.center
        return headerView
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("(i) is pressed at cell: \(indexPath)")
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("USER CHOOSE \(fruits[indexPath.row].name)")
        // TO ADD DESELECT ANIMATION
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Trunk"
    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let config = UISwipeActionsConfiguration(actions: [
            UIContextualAction(style: .normal, title: "Test", handler: { _, _, _ in
                print("TEST ACTION IS CALLED")
            }),
            UIContextualAction(style: .destructive, title: "Test 2", handler: { _, _, _ in
                print("TEST 2 ACTION IS CALLED")
            })
        ])
        return config
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fruits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TestCell", for: indexPath)
//        let element = fruits[indexPath.row]
//        cell.textLabel?.text = element.name
//        cell.detailTextLabel?.text = "\(element.expirationDate)"
////
//        if cellSwitch.isOn {
//            cell.textLabel?.text = element.name
//            cell.detailTextLabel?.text = "\(element.expirationDate)"
////            cell.textLabel?.text = "\(element) SECTION: \(indexPath.section) ROW: \(indexPath.row)"
//        }
        
        /// CUSTOM CELL
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TestCell", for: indexPath) as? MainTableViewCell else {
            fatalError("CELL IS NOT MainTableViewCell")
        }
        cell.mainLabel.text = fruits[indexPath.row].name
        cell.didChangeSwitchValue = { [weak self] isOn in
            guard let self = self else { return }
            let element = self.fruits[indexPath.row]
            let newElement = FruitStruct(name: element.name, expirationDate: element.expirationDate, isAccessible: isOn)
            self.fruits.remove(at: indexPath.row)
            self.fruits.insert(newElement, at: indexPath.row)
        }
        return cell
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 3
//    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section Header for \(section)"
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return section % 2 == 0 ? "Footer number: \(section)" : nil
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true//indexPath.row % 2 == 0
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            print("ROW AT \(indexPath) deleted")
            // REMOVE ELEMENT FROM ARRAY
            fruits.remove(at: indexPath.row)
            // REMOVE CELL FROM TABLEVIEW
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            let fruit = fruits[indexPath.row]
            fruits.insert(fruit, at: 0)
            tableView.insertRows(at: [IndexPath(item: 0, section: 0)], with: .automatic)
        default: return
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let elementMoved = fruits[sourceIndexPath.row]
        fruits.remove(at: sourceIndexPath.row)
        
        fruits.insert(elementMoved, at: destinationIndexPath.row)
    }
}
