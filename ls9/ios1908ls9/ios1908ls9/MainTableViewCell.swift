//
//  MainTableViewCell.swift
//  ios1908ls9
//
//  Created by Wa on 30.09.2021.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!

    var didChangeSwitchValue: ((Bool) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("CEll is awaked")
    }
    
    override func prepareForReuse() {
        textLabel?.text = "NO DATA"
        didChangeSwitchValue = nil
    }

    @IBAction func valueChangeAction(_ sender: UISwitch) {
        didChangeSwitchValue?(sender.isOn)
    }
}
