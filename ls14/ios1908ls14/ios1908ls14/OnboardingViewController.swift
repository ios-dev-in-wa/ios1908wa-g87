//
//  OnboardingViewController.swift
//  ios1908ls14
//
//  Created by Wa on 19.10.2021.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    var model: OnboardingModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let model = model {
            imageView.image = UIImage(systemName: model.imageName)
            titleLabel.text = model.title
            descriptionLabel.text = model.description
        }
        
    }

}
