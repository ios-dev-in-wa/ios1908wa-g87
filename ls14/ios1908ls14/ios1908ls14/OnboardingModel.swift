//
//  OnboardingModel.swift
//  ios1908ls14
//
//  Created by Wa on 19.10.2021.
//

import Foundation

struct OnboardingModel {
    let imageName: String
    let title: String
    let description: String
}
