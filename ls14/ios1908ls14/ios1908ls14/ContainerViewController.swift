//
//  ViewController.swift
//  ios1908ls14
//
//  Created by Wa on 19.10.2021.
//

import UIKit

class ContainerViewController: UIViewController {
    
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    var pageViewController: UIPageViewController?
    let onboardingModels = [
        OnboardingModel(imageName: "person", title: "Welcome!", description: "You can user this app to upload your pictures and share them with friends"),
        OnboardingModel(imageName: "person.fill", title: "Filtering!", description: "Feel free to use custom, innovative filters that are available only in our fun app!"),
        OnboardingModel(imageName: "calendar", title: "Calendar!", description: "Add custom events and have fun with friends inside our App!"),
//        OnboardingModel(imageName: "calendar", title: "Calendar!", description: "Add custom events and have fun with friends inside our App!")
    ]
    
    lazy var onboardingViewControllers: [UIViewController] = {
        let onboardingVCs: [UIViewController] = onboardingModels.compactMap { model in
            let vc = storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController
            vc?.model = model
            return vc
        }
        pageControll.numberOfPages = onboardingVCs.count
        return onboardingVCs
    }()

    @IBAction func skipAction(_ sender: UIButton) {
        print("SHOW MAIN PAGE")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPageVC", let pageVC = segue.destination as? UIPageViewController {
            pageViewController = pageVC

            pageViewController?.delegate = self
            pageViewController?.dataSource = self
            pageViewController?.setViewControllers([onboardingViewControllers.first!], direction: .forward, animated: true, completion: nil)
        }
    }

    func updateButton(index: Int) {
        let buttonTitle = index == onboardingModels.count - 1 ? "Done" : "Skip"
        skipButton.setTitle(buttonTitle, for: .normal)
    }
    
    @IBAction func pageControllValueChangedAction(_ sender: UIPageControl) {
        guard let vc = pageViewController?.viewControllers?.first,
              let index = onboardingViewControllers.firstIndex(of: vc) else { return }
        let direction: UIPageViewController.NavigationDirection = index < sender.currentPage ? .forward : .reverse
        let vcToPresent = onboardingViewControllers[sender.currentPage]
        pageViewController?.setViewControllers([vcToPresent], direction: direction, animated: true, completion: nil)
        updateButton(index: sender.currentPage)
    }
}

extension ContainerViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let vc = pageViewController.viewControllers?.first else { return }
        let index = onboardingViewControllers.firstIndex(of: vc)!
        pageControll.currentPage = index
        updateButton(index: index)
    }
}

extension ContainerViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = onboardingViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        if index == 0 {
            return nil
        }
        return onboardingViewControllers[index - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = onboardingViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        if index + 1 == onboardingViewControllers.count {
            return nil
        }
        return onboardingViewControllers[index + 1]
    }
    
}
