//
//  TestViewController.swift
//  ios1908ls14
//
//  Created by Wa on 19.10.2021.
//

import UIKit
import WebKit
import SafariServices

class TestViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

//        let url = Bundle.main.url(forResource: "image", withExtension: "png")
//        webView.loadFileURL(url!, allowingReadAccessTo: url!)
        webView.load(URLRequest(url: URL(string: "https://youtube.com")!))
    }

    override func viewDidAppear(_ animated: Bool) {
        let vc = SFSafariViewController(url: URL(string: "https://youtube.com")!)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension TestViewController: SFSafariViewControllerDelegate {
    
}
