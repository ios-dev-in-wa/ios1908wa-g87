//
//  User.swift
//  ios1908ls17
//
//  Created by Wa on 28.10.2021.
//

import Foundation

struct User {
    let name: String
    let surname: String
}
