//
//  LoginPresenter.swift
//  ios1908ls17
//
//  Created by Wa on 28.10.2021.
//

import Foundation

protocol LoginPresenterProtocol {
    func login(username: String, password: String)
    var errorReceived: ((String) -> Void)? { get set }
    var authCompleted: ((User) -> Void)? { get set }
}

class LoginPresenter: LoginPresenterProtocol {
    var errorReceived: ((String) -> Void)?
    var authCompleted: ((User) -> Void)?
    
    private let service: AuthServiceProtocol = AuthService()
    private let tokenStore: TokenStoreProtocol = TokenStore()
//    private let validator:
    
    func login(username: String, password: String) {
//        validator.validate(username, password)
        service.authentificate(username: username, password: password) { userResponse in
            guard let userResponse = userResponse else {
                errorReceived?("Credentials is not correct!")
                return
            }
            tokenStore.saveToken(token: userResponse.token)
            let user = User(name: userResponse.name, surname: userResponse.surname)
            authCompleted?(user)
        }
    }
}
