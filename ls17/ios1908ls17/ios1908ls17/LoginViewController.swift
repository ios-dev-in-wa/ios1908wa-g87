//
//  ViewController.swift
//  ios1908ls17
//
//  Created by Wa on 28.10.2021.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!

    lazy var presenter: LoginPresenterProtocol = LoginPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.errorReceived = { [weak self] errorText in
            self?.errorLabel.text = errorText
            self?.errorLabel.isHidden = false
        }
        presenter.authCompleted = { user in
            // present main VC
        }
    }

    @IBAction func loginAction(_ sender: Any) {
        presenter.login(username: loginTextField.text!, password: passwordTextField.text!)
    }
}

