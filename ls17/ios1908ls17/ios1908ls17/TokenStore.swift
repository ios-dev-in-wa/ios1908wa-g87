//
//  TokenStore.swift
//  ios1908ls17
//
//  Created by Wa on 28.10.2021.
//

import Foundation

protocol TokenStoreProtocol {
    func saveToken(token: String)
    func removeToken(token: String)
    func getToken() -> String?
}

class TokenStore: TokenStoreProtocol {
    private let defaults = UserDefaults.standard

    func saveToken(token: String) {
        defaults.set(token, forKey: "token")
    }
    
    func removeToken(token: String) {
        defaults.removeObject(forKey: "token")
    }
    
    func getToken() -> String? {
        defaults.string(forKey: "token")
    }
}

class TokenCoreDataStore: TokenStoreProtocol {
    func saveToken(token: String) {
        
    }
    
    func removeToken(token: String) {
        
    }
}
