//
//  AuthService.swift
//  ios1908ls17
//
//  Created by Wa on 28.10.2021.
//

import Foundation

struct UserResponse {
    let token: String
    let name: String
    let surname: String
}

protocol AuthServiceProtocol {
    func authentificate(username: String, password: String, completion: ((UserResponse?) -> Void))
}

class AuthService: AuthServiceProtocol {
    
    let someString = "TEST"
    func authentificate(username: String, password: String, completion: ((UserResponse?) -> Void)) {
        // MAKE A REQUEST
        if username == "bazinga" && password == "123321" {
            completion(UserResponse(token: "asdasd131r0odfs", name: "Artem", surname: "Velykyy"))
        } else {
            completion(nil)
        }
    }
}
