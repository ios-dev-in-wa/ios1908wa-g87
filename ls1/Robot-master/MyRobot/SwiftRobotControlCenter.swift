//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L3C" //  Level name
        super.viewDidLoad()
    }
    
    override func run() {
        solvePuzzle()
    }
    
    func solvePuzzle() {
        // Цикл
        findPeak()
        moveToPeak()
        uTurn()
        moveDownThePeak()
        // конец цикла
    }
    
    func findPeak() {
        
    }
    
    func moveToPeak() {
        
    }
    
    func uTurn() {
        
    }
    
    func moveDownThePeak() {
        
    }
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
    
    func doubleMove() {
        move()
        move()
    }

    func ifExample() {
        if frontIsClear {
            move()
        } else {
            turnLeft()
        }
        
        //
        
        if frontIsClear, noCandyPresent {
            doubleMove()
        }
    }
    
    func forExample() {
        // выполнит код 0, 1, 2, 3, 4 === 5 раз
        for _ in 0...4 {
            move()
        }
    }
    
    // This func makes robot move forward while no candy present
    func breakExample() {
        while frontIsClear {
            move()
            if candyPresent {
                break
                //                pick()
                //                turnRight()
            }
            
        }
//        move()
    }
}
