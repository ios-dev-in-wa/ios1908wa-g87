//
//  RawMarketService.swift
//  ios1908ls12
//
//  Created by Wa on 14.10.2021.
//

import Foundation

protocol MarketService: AnyObject {
    func getCars(completion: @escaping ((Result<[Car], Error>) -> Void))
    func saveCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void))
    func update(car: Car, completion: @escaping ((Result<Car, Error>) -> Void))
    func delete(car: Car, completion: @escaping ((Result<Void, Error>) -> Void))
}

class RawMarketService: MarketService {
    
    private let session = URLSession.shared

    private let baseUrl: URL = URL(string: "https://parseapi.back4app.com/classes/Car")!
    
    func getCars(completion: @escaping ((Result<[Car], Error>) -> Void)) {
        let request = getRequest(url: baseUrl, method: "GET", data: nil)
        session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 {
                if let data = data {
                    do {
                        let carsResponse = try JSONDecoder().decode(CarsResponse.self, from: data)
                        completion(.success(carsResponse.results))
                    } catch {
                        completion(.failure(error))
                    }
                }
            }
        }
        .resume()
    }
    
    func saveCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        do {
            let data = try JSONEncoder().encode(car)
            let request = getRequest(url: baseUrl, method: "POST", data: data)
            session.dataTask(with: request) { data, response, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let response = response as? HTTPURLResponse else { return }
                if response.statusCode == 200 || response.statusCode == 201 {
                    if let data = data {
                        do {
                        let carResposne = try JSONDecoder().decode(CarCreateResponse.self, from: data)
                            completion(.success(car.addObjectId(objId: carResposne.objectId)))
                        } catch {
                            completion(.failure(error))
                        }
                    }
                }
            }
            .resume()
        } catch {
            completion(.failure(error))
        }
    }
    
    func update(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        do {
            let data = try JSONEncoder().encode(car)
            let request = getRequest(url: baseUrl.appendingPathComponent(car.objectId!), method: "PUT", data: data)
            session.dataTask(with: request) { data, response, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let response = response as? HTTPURLResponse else { return }
                if response.statusCode == 200 || response.statusCode == 201 {
                    completion(.success(car))
                }
            }
            .resume()
        } catch {
            completion(.failure(error))
        }
    }
    
    func delete(car: Car, completion: @escaping ((Result<Void, Error>) -> Void)) {
        let request = getRequest(url: baseUrl.appendingPathComponent(car.objectId!), method: "DELETE", data: nil)
        session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 || response.statusCode == 201 {
                completion(.success(()))
            }
        }
        .resume()
    }
    
    private func getRequest(url: URL, method: String, data: Data?) -> URLRequest {
//        X-Parse-Application-Id: LoslvqRW9ZoNAexpSlyq2Ze4958oGsan5ykKYXIz
//        X-Parse-REST-API-Key: 5N2tXWoWLOMHbpTGTKFvA6xeRJmpsT33yxHbuGaZ
//        Content-Type: application/json
        var request = URLRequest(url: url)
        request.httpBody = data
        request.httpMethod = method
        request.addValue("LoslvqRW9ZoNAexpSlyq2Ze4958oGsan5ykKYXIz", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("5N2tXWoWLOMHbpTGTKFvA6xeRJmpsT33yxHbuGaZ", forHTTPHeaderField: "X-Parse-REST-API-Key")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        return request
    }
}
