//
//  CarsResponse.swift
//  ios1908ls12
//
//  Created by Wa on 14.10.2021.
//

import Foundation

struct CarsResponse: Codable {
    let results: [Car]
}
