//
//  CarCreateResponse.swift
//  ios1908ls12
//
//  Created by Wa on 14.10.2021.
//

import Foundation

struct CarCreateResponse: Codable {
    let objectId: String
    let createdAt: String
}
