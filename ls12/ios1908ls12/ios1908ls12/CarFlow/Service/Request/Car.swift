//
//  Car.swift
//  ios1908ls12
//
//  Created by Wa on 12.10.2021.
//

import Foundation
import Parse

struct Car: Codable {
    let model: String
    let productionYear: Int
    let wasInAccident: Bool
    let carDescription: String
    let objectId: String?

    init(model: String, productionYear: Int, wasInAccident: Bool, carDescription: String, objectId: String?) {
        self.model = model
        self.productionYear = productionYear
        self.wasInAccident = wasInAccident
        self.carDescription = carDescription
        self.objectId = objectId
    }
    
    init?(object: PFObject) {
        guard let model = object["model"] as? String,
              let productionYear = object["productionYear"] as? Int,
              let wasInAccident = object["wasInAccident"] as? Bool,
              let carDescription = object["carDescription"] as? String,
              let objectId = object.objectId else { return nil }
        self.model = model
        self.carDescription = carDescription
        self.objectId = objectId
        self.productionYear = productionYear
        self.wasInAccident = wasInAccident
    }

    var pfObject: PFObject {
        let pfObject = PFObject(className: "Car")
        pfObject["model"] = model
        pfObject["productionYear"] = productionYear
        pfObject["wasInAccident"] = wasInAccident
        pfObject["carDescription"] = carDescription
        return pfObject
    }

    func addObjectId(objId: String) -> Car {
        return Car(model: model, productionYear: productionYear, wasInAccident: wasInAccident, carDescription: carDescription, objectId: objId)
    }
}
