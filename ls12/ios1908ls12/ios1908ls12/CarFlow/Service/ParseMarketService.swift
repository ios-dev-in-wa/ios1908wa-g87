//
//  ParseMarketService.swift
//  ios1908ls12
//
//  Created by Wa on 12.10.2021.
//

import Foundation
import Parse

class ParseMarketService: MarketService {

    // RESULT
    // READ
    func getCars(completion: @escaping ((Result<[Car], Error>) -> Void)) {
        
        PFQuery(className: "Car").findObjectsInBackground { objects, error in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(error))
                return
            }
            guard let objects = objects else { return }
            /// TRAINE
//            var cars = [Car]()
//            for obj in objects {
//                if let car = Car(object: obj) {
//                    cars.append(car)
//                }
//            }
            /// JUN+
//            let cars = objects.map(Car.init)
            let cars = objects.compactMap(Car.init)
            completion(.success(cars))
            // FULL
//            objects.compactMap { obj in
//                return Car(object: obj)
//            }
            
        }
    }

    // CREATE
    func saveCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        let obj = car.pfObject

        obj.saveInBackground { isSaved, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            if isSaved {
                completion(.success(Car.init(object: obj)!))
            }
        }
    }

    // UPDATE
    func update(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        let pfQuery = PFQuery(className: "Car")
        
        pfQuery.getObjectInBackground(withId: car.objectId!) { obj, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let obj = obj else { return }
            obj["model"] = car.model
            obj["productionYear"] = car.productionYear
            obj["wasInAccident"] = car.wasInAccident
            obj["carDescription"] = car.carDescription

            obj.saveInBackground { isSuccess, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                if isSuccess {
                    completion(.success(Car(object: obj)!))
                }
            }
        }
    }
    
    // DELETE
    func delete(car: Car, completion: @escaping ((Result<Void, Error>) -> Void)) {
        let pfQuery = PFQuery(className: "Car")

        pfQuery.getObjectInBackground(withId: car.objectId!) { obj, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            obj?.deleteInBackground(block: { isSuccess, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                if isSuccess {
                    completion(.success(()))
                }
            })
        }
    }
}
