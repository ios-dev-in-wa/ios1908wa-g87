//
//  MarketViewController.swift
//  ios1908ls12
//
//  Created by Wa on 12.10.2021.
//

import UIKit

class MarketViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var cars: [Car] = []
    private let service: MarketService = RawMarketService()//ParseMarketService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        
//        let obj = 9x

        service.getCars { [weak self] result in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showError(error: error)
                }
            case .success(let cars):
                self?.cars = cars
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toCarVC":
            if let destinationVC = segue.destination as? CarViewController {
                destinationVC.didSaveAction = { [weak self] car in
                    self?.service.saveCar(car: car) { result in
                        switch result {
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showError(error: error)
                            }
                        case .success(let newCar):
                            self?.cars.append(newCar)
                            DispatchQueue.main.async {
                                self?.tableView.reloadData()
                            }
                        }
                    }
                }
            }
        case "toCarDetails":
            if let destinationVC = segue.destination as? CarViewController,
               let car = sender as? Car {
                destinationVC.selectedCar = car
                destinationVC.didSaveAction = { [weak self] car in
                    self?.service.update(car: car) { result in
                        switch result {
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self?.showError(error: error)
                            }
                        case .success(let newCar):
//                            self?.cars.sort(by: { carOne, carTwo in
//                                return carOne.productionYear < carTwo.productionYear
//                            })

//                            self?.cars.sort(by: { $0.productionYear < $1.productionYear })
                            
//                            self?.cars.first(where: { car in
//
//                            })
                            if let index = self?.cars.firstIndex(where: { $0.objectId == newCar.objectId }) {
                                self?.cars.remove(at: index)
                                self?.cars.insert(newCar, at: index)
                            }
                            DispatchQueue.main.async {
                                self?.tableView.reloadData()
                            }
                        }
                    }
                }
            }
        default: break
        }
    }

    private func showError(error: Error) {
        let alertVc = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alertVc.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertVc, animated: true, completion: nil)
    }
}

extension MarketViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toCarDetails", sender: cars[indexPath.row])
    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let config = UISwipeActionsConfiguration(actions: [UIContextualAction(style: .destructive, title: "Remove", handler: { [weak self] _, _, _ in
            guard let self = self else { return }
            self.service.delete(car: self.cars[indexPath.row], completion: { result in
                switch result {
                case .failure(let error):
                    DispatchQueue.main.async {
                        self.showError(error: error)
                    }
                case .success(_):
                    self.cars.remove(at: indexPath.row)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            })
        })])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
}

extension MarketViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CarTableViewCell", for: indexPath) as? CarTableViewCell else { fatalError() }
        cell.setup(car: cars[indexPath.row])
        return cell
    }
}
