//
//  CarTableViewCell.swift
//  ios1908ls12
//
//  Created by Wa on 12.10.2021.
//

import UIKit

class CarTableViewCell: UITableViewCell {

    @IBOutlet weak var modelAndYearLabel: UILabel!
    @IBOutlet weak var wasInAccidentLabel: UILabel!
    @IBOutlet weak var carDescriptionLabel: UILabel!

    func setup(car: Car) {
        modelAndYearLabel.text = car.model + " \(car.productionYear)"
        wasInAccidentLabel.text = car.wasInAccident ? "Car is restored" : "Car wan't in accident"
        carDescriptionLabel.text = car.carDescription
    }
}
