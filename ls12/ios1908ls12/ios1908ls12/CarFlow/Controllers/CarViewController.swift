//
//  CarViewController.swift
//  ios1908ls12
//
//  Created by Wa on 12.10.2021.
//

import UIKit

class CarViewController: UIViewController {

    @IBOutlet weak var modelNameTextField: UITextField!
    @IBOutlet weak var productionYearTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var inAccidentSwitch: UISwitch!
    let button = UIButton()

    var selectedCar: Car?
    var didSaveAction: ((Car) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let car = selectedCar {
            modelNameTextField.text = car.model
            productionYearTextField.text = "\(car.productionYear)"
            descriptionTextField.text = car.carDescription
            inAccidentSwitch.isOn = car.wasInAccident
        }
        button.addTarget(self, action: #selector(hideAnhidePassword), for: .touchUpInside)
        modelNameTextField.rightView = button
    }
    
    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        guard let modelName = modelNameTextField.text,
              let productionYearString = productionYearTextField.text,
              let productionYear = Int(productionYearString),
              let decription = descriptionTextField.text else { return }
        let car = Car(model: modelName, productionYear: productionYear, wasInAccident: inAccidentSwitch.isOn, carDescription: decription, objectId: selectedCar?.objectId)
        didSaveAction?(car)
        navigationController?.popViewController(animated: true)
    }

    @objc func hideAnhidePassword() {
        
    }
}
