//
//  ViewController.swift
//  ios1908g87ls2
//
//  Created by Wa on 26.08.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        stringExample()
        letvarExample()
        castingExample()
        printName(name: "Artem")
        printName(name: "Dima")
    
        print("--------------------")
        let result = multiplier(a: 2, b: 2)
        print(result)
        if result == 4 {
            print("multiplier func works good")
        } else {
            print("multiplier func failed")
        }

        greeting(name: "Artem")
//        print("Greeting, my dear Artem")
        greeting(name: "Inna")
//        print("Greeting, my dear Inna")
        greeting(name: "Ihor")
//        print("Greeting, my dear Ihor")
        
        ariphmetica()
        shortHandsExample()
        counterExample()
        print("--------------------")
        switchExample(value: 2020)
        switchExample(value: 2022)
        switchExample(value: 1)

        addBox(x: 9, y: 10)
        addBox(x: 120, y: 120)
    }

    func addBox(x: Float, y: Float) {
        let boxView = UIView()
        
        boxView.frame = CGRect(x: Int(x), y: Int(y), width: 100, height: 100)
        if x.truncatingRemainder(dividingBy: 2) == 0 {
            boxView.backgroundColor = .blue
        } else {
            boxView.backgroundColor = .red
        }

        view.addSubview(boxView)
    }

    func letvarExample() {
        // Constant
        let studentsAmount = 11
//        let studentsAmount: Int = 11

        // Varible
        var currentStudentsAmount = 9

//        studentsAmount = 12

        currentStudentsAmount = 6//"artem"
        print(studentsAmount, currentStudentsAmount)
    }

    func stringExample() {
        let myName = "Artem"
        print(myName)

//        let isArtemLecturer = true
//        let isArtemBilousStudent = true

        let doubleEx: Double = 5
        print(doubleEx)
    }

    func castingExample() {
        let appleCount = 5

        let optional = appleCount as? String
        print(optional)

        let intToString = String(appleCount)
        print(intToString)
//        let text: String =
    }

    func printName(name: String) {
        print(name)
    }

    func multiplier(a: Int, b: Int) -> Int {
        return a * b// - 1
    }

    func greeting(name: String) {
        print("Greeting, my dear \(name)")
    }

    func ariphmetica() {
        let firstNumber = 1
        let result: Double = Double(firstNumber) / 2.0
        print(result)

        let resultIsZero = result == 0
        if resultIsZero {
            print("Result is 0")
        } else {
            print("Result is not 0")
        }
    }

    func shortHandsExample() {
        var result = 0

        while result != 10 {
            result += 1
            print("Current iterration is: \(result)")
        }
        print("Now result is 10")
    }

    func counterExample() {
        var counter = 0

        for _ in 0...20 {
            counter -= 1
        }
        print(counter)
        // -----------------
        let someVal = 10
        for i in 0...20 {
            print("iterration number is: \(someVal * i)")
        }
    }

    func switchExample(value: Int) {
//        let intVal = 100
//        print(Int.min, Int.max)
        
        // TRY NOT TO USE IT
//        if value == 2020 {
//            print("COVID IS HERE")
//        } else if value == 1945 {
//            print("Victory!")
//        } else if value == 2021 {
//            print("Beleniuk gold medal")
//        } else {
//            print("Unsupported value: \(value)")
//        }
        // BETTER
        switch value {
        case 2020:
            print("COVID IS HERE")
        case 1945:
            print("Victory!")
        case 2021:
            print("Beleniuk gold medal")
        default:
            print("Unsupported value: \(value)")
        }
    }
}

