//
//  UsersViewController.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import UIKit

class UsersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var users = [User]()
    private let localSaver = LocalSaver()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let cellNib = UINib(nibName: "MenuTableViewCell", bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: "MenuTableViewCell")
        
        tableView.dataSource = self
//        tableView.delegate = self

        tableView.tableFooterView = UIView()
        
        users = localSaver.readUsers()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToCreateUserScene",
           let destinationVC = segue.destination as? CreateUserViewController {
            destinationVC.didPressSaveUser = { [weak self] user in
                self?.users.insert(user, at: 0)
                self?.localSaver.saveUsers(users: self?.users ?? [])
                self?.tableView.reloadSections([0], with: .automatic)
            }
        }
    }
}

extension UsersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MainTableViewCell else { fatalError() }
        let user = users[indexPath.row]
        cell.setupWith(title: user.name + user.surname, imageName: user.imageName)
        return cell
    }
    
}
