//
//  MenuItem.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import Foundation

struct MenuItem {
    let title: String
    let imageName: String
    let segueIdentifier: String?
}
