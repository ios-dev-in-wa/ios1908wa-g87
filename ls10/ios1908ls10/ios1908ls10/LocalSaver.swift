//
//  LocalSaver.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import Foundation

class LocalSaver {

    let fileManger = FileManager.default
    
    private var usersListUrl: URL {
        return fileManger.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("users.txt")
    }

    func saveUsers(users: [User]) {
//        let documentsDir = fileManger.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        print(documentsDir)
//        let usersListUrl = documentsDir.appendingPathComponent("users.txt")
//        do {
//            try user.name.write(to: usersListUrl, atomically: true, encoding: .utf8)
//            print("Success")
//        } catch {
//            print(error)
//        }
        do {
            let data = try JSONEncoder().encode(users)
            try data.write(to: usersListUrl)
        } catch {
            print(error)
        }
    }
    
    func readUsers() -> [User] {
        do {
            let data = try Data(contentsOf: usersListUrl)
            let users = try JSONDecoder().decode([User].self, from: data)
            print(users)
            return users
        } catch {
            print(error)
        }
        return []
    }
}
