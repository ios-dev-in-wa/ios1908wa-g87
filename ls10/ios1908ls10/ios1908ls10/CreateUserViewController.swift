//
//  CreateUserViewController.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import UIKit

class CreateUserViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var imageNameTextField: UITextField!

    var didPressSaveUser: ((User) -> Void)?

    @IBAction func saveUserAction(_ sender: UIButton) {
        guard let name = nameTextField.text,
              let surname = surnameTextField.text,
              let imageName = imageNameTextField.text else { return }
        let user = User(name: name, surname: surname, imageName: imageName)
        didPressSaveUser?(user)
        dismiss(animated: true, completion: nil)
    }
}
