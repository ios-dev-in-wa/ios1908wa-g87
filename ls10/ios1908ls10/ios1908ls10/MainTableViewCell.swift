//
//  MenuTableViewCell.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import UIKit

// Access modifiers
// 'INTERNAL' - default modifier
// 'PUBLIC' - every project can get access
// 'PRIVATE' - deny access from other classes

class MainTableViewCell: UITableViewCell {

    @IBOutlet private var cellImageView: UIImageView!
    @IBOutlet private var mainLabel: UILabel!
    
    func setupWith(title: String, imageName: String) {
        cellImageView.image = UIImage(systemName: imageName)
        mainLabel.text = title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
