//
//  ViewController.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var menuItems: [MenuItem] = [
        MenuItem(title: "Profile", imageName: "person", segueIdentifier: nil),
        MenuItem(title: "Users", imageName: "book.fill", segueIdentifier: "GoToUsersScene"),
        MenuItem(title: "Settings", imageName: "gear", segueIdentifier: "GoToSettings")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "MenuTableViewCell", bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: "MenuTableViewCell")
        
        tableView.dataSource = self
        tableView.delegate = self

        tableView.tableFooterView = UIView()
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MainTableViewCell else { fatalError() }
        let item = menuItems[indexPath.row]
        cell.setupWith(title: item.title, imageName: item.imageName)
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = menuItems[indexPath.row]
        
        if let segueId = menuItem.segueIdentifier {
            performSegue(withIdentifier: segueId, sender: nil)
        }
    }
}
