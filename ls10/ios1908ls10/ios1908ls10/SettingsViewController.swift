//
//  SettingsViewController.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var menuItems: [MenuItem] = [
        MenuItem(title: "System", imageName: "gear", segueIdentifier: nil),
        MenuItem(title: "Log out", imageName: "person.fill", segueIdentifier: nil)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "MenuTableViewCell", bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: "MenuTableViewCell")
        
        tableView.dataSource = self
        tableView.delegate = self

        tableView.tableFooterView = UIView()
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch menuItems[indexPath.row].title {
        case "Log out":
            let alertVC = UIAlertController(title: "Warning", message: "You are going to log out. Are you sure?", preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "Sure", style: .destructive, handler: { _ in
                print("Loggin out")
            }))
            alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alertVC, animated: true, completion: nil)
        default:
                break
        }
    }
}

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MainTableViewCell else { fatalError() }
        let item = menuItems[indexPath.row]
        cell.setupWith(title: item.title, imageName: item.imageName)
        return cell
    }
}
