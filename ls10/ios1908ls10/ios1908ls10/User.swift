//
//  User.swift
//  ios1908ls10
//
//  Created by Wa on 05.10.2021.
//

import Foundation

struct User: Codable {
    let name: String
    let surname: String
    let imageName: String
}
