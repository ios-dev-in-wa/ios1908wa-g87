//
//  SecondViewController.swift
//  ios1908ls11
//
//  Created by Wa on 07.10.2021.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var mainLabel: UILabel!
    
    var text: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainLabel.text = text
    }
}
