//
//  Settings.swift
//  ios1908ls11
//
//  Created by Wa on 07.10.2021.
//

import UIKit

enum UserFont: Int {
    case font1, font2, font3

    var font: UIFont {
        switch self {
        case .font1: return UIFont(name: "Times New Roman", size: 16)!
        default: return UIFont(name: "Times New Roman", size: 16)!
        }
    }
}

final class Settings {
    
    private init() { }
    
    static let shared = Settings()
    
    private let defaults = UserDefaults.standard
    
    var userSessionAmount: Int {
        get {
            return defaults.integer(forKey: "UserSessionAmount")
        }
        set {
            defaults.setValue(newValue, forKeyPath: "UserSessionAmount")
        }
    }
    
    var userFont: UserFont {
        get {
            let rawValue = defaults.value(forKey: "userFont") as! Int
            return UserFont(rawValue: rawValue)!
        }
        set {
            defaults.setValue(newValue.rawValue, forKeyPath: "userFonr")
        }
    }
    
    
}
