//
//  DrawViewController.swift
//  ios1908ls11
//
//  Created by Wa on 07.10.2021.
//

import UIKit
import FaceCropper
import NVActivityIndicatorView

class DrawViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
//
//    static func test() {
//
//    }
//
//    static var testVar = "tt"
//
//    final func notForChild() {
//
//    }
    
    var faces = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()

//        let drawView = DrawView(frame: CGRect(x: 10, y: 10, width: 300, height: 400))
//        view.addSubview(drawView)
//        Settings.shared.userSessionAmount
        
        UIImage(named: "testImage")?.face.crop({ result in
            switch result {
            case .success(let images):
                self.faces = images
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            case .notFound:
                print("Not found faces")
            }
        })
        
        // UIColor(named: "AppColor") COLOR FROM ASSETS
        tableView.dataSource = self
        
        let indicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), type: .pacman, color: .black, padding: nil)
        
        view.addSubview(indicatorView)
        indicatorView.center = view.center
        indicatorView.startAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DrawViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell else { fatalError() }
        cell.userImageView.image = faces[indexPath.row]
        return cell
    }
    
    
}
