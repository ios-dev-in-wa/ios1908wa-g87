//
//  DrawView.swift
//  ios1908ls11
//
//  Created by Wa on 07.10.2021.
//

import UIKit

class DrawView: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        UIColor.white.setFill()
        UIRectFill(rect)
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 10, y: 10))
        path.addLine(to: CGPoint(x: 10, y: 110))
//        path.addLine(to: CGPoint(x: 110, y: 110))
        path.addLine(to: CGPoint(x: 110, y: 10))
        path.addLine(to: CGPoint(x: 10, y: 10))
        path.close()

        path.lineWidth = 2
        
        UIColor.red.setFill()
        UIColor.blue.setStroke()
        
        path.fill()
        path.stroke()
     
        backgroundColor = .clear
    }

}
