//
//  ViewController.swift
//  ios1908ls11
//
//  Created by Wa on 07.10.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSecondVC",
           let vc = segue.destination as? SecondViewController {
            vc.text = textField.text ?? "NO TEXT"

        }
    }

    @IBAction func buttonAction(_ sender: UIButton) {
        // FIRST
//        performSegue(withIdentifier: "showSecondVC", sender: nil)
        // -------------
        //
        if let secondVC = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
            secondVC.text = textField.text ?? "NO TEXT"
            // MODAL
//            present(secondVC, animated: true, completion: nil)
            
            // NAV STACK
            navigationController?.pushViewController(secondVC, animated: true)
        }
    }
    
}

